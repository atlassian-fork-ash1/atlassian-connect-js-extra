var gulp = require('gulp'),
    nightwatch = require('gulp-nightwatch');

var saucelabs = process.env.SAUCE_LABS || false;
module.exports = function() {
  return function(){
    var cliArgs = ['--verbose'];
    if(saucelabs) {
      cliArgs.push('--env saucelabs');
    }

    return gulp.src('gulpfile.js').pipe(nightwatch({
      configFile: 'nightwatch.json',
      cliArgs: cliArgs
    }));
  };
};