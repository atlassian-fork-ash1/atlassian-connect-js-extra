describe('history plugin', () => {
  const historyStub = {
    _registerWindowListeners: jasmine.createSpy('register window listeners'),
    getState: jasmine.createSpy('get state')
  }

  const AP = {
    _hostModules: {
      history: historyStub
    },
    register: jasmine.createSpy('register'),
    history: historyStub,
    _data: {
      options: {
        history: {
          state: 'somestate'
        }
      }
    }
  };

  window.AP = AP;
  require('../../src/plugin/index');

  beforeEach(() => {
    window.AP = AP;
  });

  it('calls AP.register', () => {
    expect(AP.register).toHaveBeenCalled();
  });

  it('sets popState', () => {
    expect(AP._hostModules.history.popState).toBeDefined();
    expect(AP.history.popState).toBeDefined();
  });

  it('popState calls _registerWindowListeners', () => {
    AP.history.popState(function () {});
    expect(AP.history._registerWindowListeners).toHaveBeenCalled();
  });

  it('sets subscribeState', () => {
    expect(AP._hostModules.history.subscribeState).toBeDefined();
    expect(AP.history.subscribeState).toBeDefined();
  });

  it('subscribeState calls _registerWindowListeners', () => {
    AP.history.subscribeState('change', function () {});
    expect(AP.history._registerWindowListeners).toHaveBeenCalled();
  });

  it('sets unsubscribeState', () => {
    expect(AP._hostModules.history.unsubscribeState).toBeDefined();
    expect(AP.history.unsubscribeState).toBeDefined();
  });

  it('immediately gets state', () => {
    expect(AP.history.getState()).toEqual('somestate');
  });

});