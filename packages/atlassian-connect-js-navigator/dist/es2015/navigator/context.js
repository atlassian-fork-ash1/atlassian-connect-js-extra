"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NavigatorContext {
    constructor() {
        this.contextFn = false;
    }
    // the setContextFunction is called in the Confluence / JIRA code:
    // https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence/browse/confluence-plugins/confluence-bundled-plugins/confluence-web-resources/src/main/resources/includes/js/api/navigator-context.js
    setContextFunction(fn) {
        this.contextFn = fn;
    }
    getContextFunction() {
        return this.contextFn;
    }
}
exports.default = new NavigatorContext();
