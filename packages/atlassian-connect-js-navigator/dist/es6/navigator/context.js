var NavigatorContext = /** @class */ (function () {
    function NavigatorContext() {
        this.contextFn = false;
    }
    // the setContextFunction is called in the Confluence / JIRA code:
    // https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence/browse/confluence-plugins/confluence-bundled-plugins/confluence-web-resources/src/main/resources/includes/js/api/navigator-context.js
    NavigatorContext.prototype.setContextFunction = function (fn) {
        this.contextFn = fn;
    };
    NavigatorContext.prototype.getContextFunction = function () {
        return this.contextFn;
    };
    return NavigatorContext;
}());
export default new NavigatorContext();
