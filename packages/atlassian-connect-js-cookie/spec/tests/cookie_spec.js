import cookie from '../../src/host';

const cookieReadReturnValue = 'test cookie';

describe('cookie', () => {

  beforeEach(() => {
    // delete all cookies between tests
    document.cookie.split(';').forEach(function(c) {
      document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
    });
  });

  describe('save', () => {
    it('saves a cookie', () => {
      const addonKey = 'addon_key';
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      };

      cookie.save(name, value, expires, callback);
      expect(cookie.read(name, callback)).toEqual(value);
    });

    it('saves multiple cookies', () => {
      const addonKey = 'addon_key';
      const name = 'test';
      const name2 = 'test2';
      const value = 'value123';
      const value2 = 'value1234';
      const expires = 123;
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      }

      cookie.save(name, value, expires, callback);
      cookie.save(name2, value2, expires, callback);
      expect(cookie.read(name, callback)).toEqual(value);
      expect(cookie.read(name2, callback)).toEqual(value2);
    });

    it('allows 2 cookies with the same name from diffent addon keys', () => {
      const addonKey = 'addon_key';
      const addonKey2 = 'addon_key2';
      const name = 'test';
      const value = 'value123';
      const value2 = 'anothervalue';
      const expires = 123;
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      };
      const callback2 = function(){};
      callback2._context = {
        extension: {
          addon_key: addonKey2
        }
      };

      cookie.save(name, value, expires, callback);
      cookie.save(name, value2, expires, callback2);
      expect(cookie.read(name, callback)).toEqual(value);
      expect(cookie.read(name, callback2)).toEqual(value2);
    });

    it('throws an error when callback is missing', () => {
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      try {
        cookie.save(name, value, expires, null);
      } catch (e) {
        expect(e.message).toEqual('addon key not found in callback');
      }
      expect(document.cookie).toEqual('');
    });

    it('throws an error when addonkey is undefined', () => {
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      const callback = function(){};
      callback._context = {
        extension: {}
      };
      try {
        cookie.save(name, value, expires, callback);
      } catch (e) {
        expect(e.message).toEqual('addon key must be defined on cookies');
      }
      expect(document.cookie).toEqual('');
    });

    it('throws an error when cookie name is undefined', () => {
      const addonKey = 'addon_key';
      const value = 'value123';
      const expires = 123;
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      };
      try {
        cookie.save(null, value, expires, callback);
      } catch (e) {
        expect(e.message).toEqual('Name must be defined');
      }
      expect(document.cookie).toEqual('');
    });

  });

  describe('read', () => {
    function getCookieCallback(addonKey){
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      };
      return callback;
    }

    it('allows cookie keys values to contain % and pipe chars', () => {
      // test % sign and encoded %25
      const cookieName1 = 'a%25b';
      const addonKey = 'percent_addon_key_one';
      const cookieValue1 = 'av%25alue1';
      cookie.save(cookieName1, cookieValue1, 10000, getCookieCallback(addonKey));
      let returnedCookieValue1 = cookie.read(cookieName1, getCookieCallback(addonKey));
      expect(returnedCookieValue1).toEqual(cookieValue1);

      // test |
      const cookieName2 = 'a|b';
      const cookieValue2 = 'av|alue2';
      cookie.save(cookieName2, cookieValue2, 10000, getCookieCallback(addonKey));
      let returnedCookieValue2 = cookie.read(cookieName2, getCookieCallback(addonKey));
      expect(returnedCookieValue2).toEqual(cookieValue2);

      // test percent encoded pipe (%7C)
      const cookieName3 = 'a%7cb';
      const cookieValue3 = 'av%7calue3';
      cookie.save(cookieName3, cookieValue3, 10000, getCookieCallback(addonKey));
      let returnedCookieValue3 = cookie.read(cookieName3, getCookieCallback(addonKey));
      expect(returnedCookieValue3).toEqual(cookieValue3);

      const correctCookieString = '|' + addonKey + '$$a**PERCENT**25b=av**PERCENT**25alue1|' + 
      addonKey + '$$a**PIPE**b=av**PIPE**alue2|' + 
      addonKey + '$$a**PIPE_PERCENT_ENCODED_LOWER**b=av**PIPE_PERCENT_ENCODED_LOWER**alue3';

      expect(document.cookie).toContain('AJS.conglomerate.cookie="' + correctCookieString + '"');
    });
    it('only allows reading of cookies set with current add-on key', () => {

      const cookieName1 = 'a';
      const addonKey1 = 'addon_key_one';
      const cookieValue1 = 'avalue';
      // saves addon_key_one cookie cookie
      cookie.save(cookieName1, cookieValue1, 10000, getCookieCallback(addonKey1));

      const cookieName2 = 'secret';
      const addonKey2 = 'addon_key_two';
      const cookieValue2 = '123456';
      // saves addon_key_two cookie cookie
      cookie.save(cookieName2, cookieValue2, 10000, getCookieCallback(addonKey2));

      let readCookieValue = cookie.read(cookieName1 + '=' + cookieValue1 + '|' + addonKey2 + '$$' + cookieName2, getCookieCallback(addonKey1));
      expect(readCookieValue).not.toEqual(cookieValue2);
    });
  });


  describe('erase', () => {

    it('erases a cookie', () => {
      const addonKey = 'addon_key';
      const name = 'test';
      const value = 'a value';
      const callback = function(){};
      callback._context = {
        extension: {
          addon_key: addonKey
        }
      };
      cookie.save(name, value, 10000, callback);
      expect(cookie.read(name, callback)).toEqual(value);
      cookie.erase(name, callback);
      expect(cookie.read(name, callback)).toEqual(undefined);
    });

  });
});