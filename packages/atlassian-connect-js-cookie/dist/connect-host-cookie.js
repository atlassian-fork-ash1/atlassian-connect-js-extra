(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.connectHostCookie = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/**
* Allows add-ons to store, retrieve and erase cookies against the host Jira / Confluence. These cannot be seen by other add-ons.
* @exports Cookie
*/

// taken in part from https://bitbucket.org/atlassian/aui/src/master/src/js/aui/cookie.js
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var COOKIE_NAME = 'AJS.conglomerate.cookie';
var UNESCAPE_COOKIE_REGEX = /(\\|^"|"$)/g;
var CONSECUTIVE_PIPE_CHARS_REGEX = /\|\|+/g;
var ANY_QUOTE_REGEX = /"/g;
var REGEX_SPECIAL_CHARS = /[.*+?|^$()[\]{\\]/g;
var PIPE_PERCENT_ENCODED_UPPER = encodeURIComponent("|"); // %7C
var PIPE_PERCENT_ENCODED_LOWER = encodeURIComponent("|").toLowerCase(); // %7c
var PIPE_REPLACEMENT_STR = '**PIPE**';
var PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER = '**PIPE_PERCENT_ENCODED_UPPER**';
var PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER = '**PIPE_PERCENT_ENCODED_LOWER**';
var PERCENT_REPLACEMENT_STR = '**PERCENT**';

function encodePipesAndPercents(str) {
  if (typeof str !== "string") {
    return str;
  }
  if (str.indexOf(PIPE_REPLACEMENT_STR) !== -1 || str.indexOf(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER) !== -1 || str.indexOf(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER) !== -1) {
    throw new Error('Cannot use cookies that contain ' + PIPE_REPLACEMENT_STR + ' or ' + PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER + PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER);
  }
  if (str.indexOf(PERCENT_REPLACEMENT_STR) !== -1) {
    throw new Error('Cannot use cookies that contain ' + PERCENT_REPLACEMENT_STR);
  }

  str = str.replace('|', PIPE_REPLACEMENT_STR);
  str = str.replace(PIPE_PERCENT_ENCODED_UPPER, PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER);
  str = str.replace(PIPE_PERCENT_ENCODED_LOWER, PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER);
  str = str.replace('%', PERCENT_REPLACEMENT_STR);
  return str;
}

function decodePipesAndPercents(str) {
  if (typeof str !== "string") {
    return str;
  }
  str = str.replace(PIPE_REPLACEMENT_STR, '|');
  str = str.replace(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER, PIPE_PERCENT_ENCODED_UPPER);
  str = str.replace(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER, PIPE_PERCENT_ENCODED_LOWER);
  str = str.replace(PERCENT_REPLACEMENT_STR, '%');
  return str;
}

function regexEscape(str) {
  return str.replace(REGEX_SPECIAL_CHARS, '\\$&');
}

function getValueFromConglomerate(name, cookieValue) {
  // A null cookieValue is just the first time through so create it.
  cookieValue = cookieValue || '';
  var reg = new RegExp(regexEscape(name) + '=([^|]+)');
  var res = cookieValue.match(reg);
  return res && res[1];
}

// Either append or replace the value in the cookie string/
function addOrAppendToValue(name, value, cookieValue) {
  // A cookie name follows after any amount of white space mixed with any amount of '|' characters.
  // A cookie value is preceded by '=', then anything except for '|'.
  var reg = new RegExp('(\\s|\\|)*\\b' + regexEscape(name) + '=[^|]*[|]*');

  cookieValue = cookieValue || '';
  cookieValue = cookieValue.replace(reg, '|');

  if (value !== '') {
    var pair = name + '=' + value;
    if (cookieValue.length + pair.length < 4020) {
      cookieValue += '|' + pair;
    }
  }

  return cookieValue.replace(CONSECUTIVE_PIPE_CHARS_REGEX, '|');
}

function unescapeCookieValue(name) {
  return name.replace(UNESCAPE_COOKIE_REGEX, '');
}

function getCookieValue(name) {
  var reg = new RegExp('\\b' + regexEscape(name) + '=((?:[^\\\\;]+|\\\\.)*)(?:;|$)');
  var res = document.cookie.match(reg);
  return res && unescapeCookieValue(res[1]);
}

function saveCookie(name, value, days) {
  var ex = '';
  var d;
  var quotedValue = '"' + value.replace(ANY_QUOTE_REGEX, '\\"') + '"';

  if (days) {
    d = new Date();
    d.setTime(+d + days * 24 * 60 * 60 * 1000);
    ex = '; expires=' + d.toGMTString();
  }

  document.cookie = name + '=' + quotedValue + ex + ';path=/';
}

function _save(name, value, expires) {
  var cookieValue = getCookieValue(COOKIE_NAME);
  cookieValue = addOrAppendToValue(name, value, cookieValue);
  saveCookie(COOKIE_NAME, cookieValue, expires || 365);
}

function _read(name, defaultValue) {
  var cookieValue = getCookieValue(COOKIE_NAME);
  var value = getValueFromConglomerate(name, cookieValue);
  if (value != null) {
    return value;
  }
  return defaultValue;
}

function _erase(name) {
  _save(name, '');
}

function prefixCookie(addonKey, name) {
  if (!addonKey || addonKey.length === 0) {
    throw new Error('addon key must be defined on cookies');
  }

  if (!name || name.length === 0) {
    throw new Error('Name must be defined');
  }
  return addonKey + '$$' + name;
}

function addonKeyFromCallback(callback) {
  if (callback && callback._context) {
    return callback._context.extension.addon_key;
  } else {
    throw new Error('addon key not found in callback');
  }
}

exports["default"] = {
  /**
  * Save a cookie.
  * @param name {String} name of cookie
  * @param value {String} value of cookie
  * @param expires {Number} number of days before cookie expires
  * @noDemo
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  */
  save: function save(name, value, expires) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), encodePipesAndPercents(name));
    if (callback._context) {
      _save(cookieName, encodePipesAndPercents(value), expires);
    }
  },
  /**
  * Get the value of a cookie.
  * @param name {String} name of cookie to read
  * @param callback {Function} callback to pass cookie data
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  */
  read: function read(name, callback) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), encodePipesAndPercents(name));
    var value = decodePipesAndPercents(_read(cookieName));
    callback(value);
    return value; // for testing
  },
  /**
  * Remove the given cookie.
  * @param name {String} the name of the cookie to remove
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  * AP.cookie.erase('my_cookie');
  */
  erase: function erase(name) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), encodePipesAndPercents(name));
    _erase(cookieName);
  }
};
module.exports = exports["default"];
},{}]},{},[1])(1)
});
