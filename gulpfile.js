var gulp = require('gulp');

function getTask(task) {
  return require('./gulp-tasks/' + task)(gulp);
}

gulp.task('karma', getTask('karma'));
gulp.task('karma-ci', getTask('karma-ci'));
gulp.task('nightwatch', getTask('nightwatch'));